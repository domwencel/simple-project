package validator

import (
	"github.com/go-playground/validator/v10"
)

type validationError struct {
	Field   string `json:"field,omitempty"`
	Message string `json:"message,omitempty"`
}

func newError(field, message string) error {
	return &validationError{
		Field:   field,
		Message: message,
	}
}

func (e *validationError) Error() string {
	return e.Message
}

func New() *validator.Validate {
	validate := validator.New()
	return validate
}

func ParseError(err error) []error {
	if fieldErrors, ok := err.(validator.ValidationErrors); ok {
		errors := make([]error, len(fieldErrors))
		for i, err := range fieldErrors {
			errors[i] = newError(err.Field(), "Invalid value")
		}
		return errors
	}

	return nil
}
