package token

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"simple-project/internal/database"
	"time"
)

type TokenNotFound struct {
	Message string
}

func (t TokenNotFound) Error() string {
	return t.Message
}

func NewTokenService(db *database.DB) *TokenServiceImpl {
	return &TokenServiceImpl{db: db}
}

type TokenServiceImpl struct {
	db *database.DB
}

type TokenService interface {
	CreateToken(ctx context.Context, userId *uuid.UUID) (*Token, error)
	GetOne(ctx context.Context, id uuid.UUID) (*Token, error)
	GetOneByUserId(ctx context.Context, userId uuid.UUID) (*Token, error)
	Revoke(ctx context.Context, token uuid.UUID) error
	UpdateTime(ctx context.Context, id uuid.UUID) error
}

func (service *TokenServiceImpl) CreateToken(ctx context.Context, userId *uuid.UUID) (*Token, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	var t Token
	err = conn.QueryRow(ctx, `
	INSERT INTO token
	VALUES ($1, $2, $3)
	RETURNING id, user_id, expire_at
	`, uuid.New(), userId, time.Now().Add(time.Hour)).Scan(&t.Id, &t.UserId, &t.ExpireAt)

	if err != nil {
		return nil, err
	}

	return &t, nil
}

func (service *TokenServiceImpl) GetOne(ctx context.Context, id uuid.UUID) (*Token, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	row := conn.QueryRow(ctx, `
	SELECT 
		id, user_id, expire_at
	FROM 
		token
	WHERE 
		id = $1
	`, id)
	token, err := scanOne(row)
	if err != nil {
		return nil, err
	}

	return token, nil
}

func (service *TokenServiceImpl) GetOneByUserId(ctx context.Context, userId uuid.UUID) (*Token, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	row := conn.QueryRow(ctx, `
	SELECT 
		id, user_id, expire_at
	FROM 
		token
	WHERE 
		user_id = $1
	`, userId)
	token, err := scanOne(row)
	if err == pgx.ErrNoRows {
		return nil, &TokenNotFound{Message: "Token Not Found"}
	}
	if err != nil {
		return nil, err
	}

	return token, nil

}

func (service *TokenServiceImpl) Revoke(ctx context.Context, token uuid.UUID) error {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	_, err = conn.Exec(ctx, `
		 DELETE FROM token WHERE id = $1
	`, token)

	if err != nil {
		return err
	}

	return nil
}

func (service *TokenServiceImpl) UpdateTime(ctx context.Context, id uuid.UUID) error {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	_, err = conn.Exec(ctx, `
		UPDATE token SET expire_at = $1 WHERE id = $2 
	`, time.Now().Add(time.Hour), id)

	if err != nil {
		return err
	}

	return nil
}

func scanOne(row pgx.Row) (*Token, error) {
	var t Token
	if err := row.Scan(&t.Id, &t.UserId, &t.ExpireAt); err != nil {
		return nil, err
	}

	return &t, nil
}
