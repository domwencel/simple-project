package token

import (
	"github.com/google/uuid"
	"time"
)

type Token struct {
	Id       uuid.UUID `json:"id"`
	UserId   uuid.UUID `json:"user_id"`
	ExpireAt time.Time `json:"expire_at"`
}

func (t Token) IsActive() bool {
	return t.ExpireAt.After(time.Now())
}
