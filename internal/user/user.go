package user

import (
	"github.com/google/uuid"
	"time"
)

type UserStatus string

const (
	UNACTIVATED UserStatus = "UNACTIVATED"
	ACTIVE                 = "ACTIVE"
	BANNED                 = "BANNED"
	DELETED                = "DELETED"
)

type User struct {
	Id        uuid.UUID `json:"id"`
	Login     string    `json:"login"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Status UserStatus `json:"status"`
	Password string `json:"-"`
}