package user

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
	"simple-project/internal/database"
)

type UserService struct {
	db *database.DB
}

func NewUserService(db *database.DB) *UserService {
	return &UserService{db: db}
}

func (service *UserService) CreateUser(ctx context.Context, email, login string) (*uuid.UUID, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	id := uuid.New()
	_, err = conn.Exec(ctx, `
	INSERT INTO USERS (id, login, email, updated_at)
	VALUES ($1, $2, $3, CURRENT_TIMESTAMP)
	`, id, login, email)

	if err != nil {
		return nil, err
	}

	return &id, nil

}

func (service *UserService) GetUserByToken(ctx context.Context, token uuid.UUID) (*User, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	row := conn.QueryRow(ctx, `
	SELECT 
		id, login, email, created_at, updated_at, status, password
	FROM 
		users
	WHERE 
		id = (SELECT user_id FROM token WHERE id = $1)
	`, token)
	user, err := scanOne(row)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (service *UserService) GetUserByLogin(ctx context.Context, login string) (*User, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	row := conn.QueryRow(ctx, `
	SELECT 
		id, login, email, created_at, updated_at, status, password
	FROM 
		users
	WHERE 
		login = $1
	`, login)
	user, err := scanOne(row)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (service *UserService) GetUserByEmail(ctx context.Context, email string) (*User, error) {
	conn, err := service.db.Pool.Acquire(ctx)
	if err != nil {
		return nil, fmt.Errorf("errors with acquiring connection: %w", err)
	}
	defer conn.Release()

	row := conn.QueryRow(ctx, `
	SELECT 
		id, login, email, created_at, updated_at, status, password
	FROM 
		users
	WHERE 
		email = $1
	`, email)
	user, err := scanOne(row)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func scanOne(row pgx.Row) (*User, error) {
	var u User
	if err := row.Scan(&u.Id, &u.Login, &u.Email, &u.CreatedAt, &u.UpdatedAt, &u.Status, &u.Password); err != nil {
		return nil, err
	}

	return &u, nil
}
