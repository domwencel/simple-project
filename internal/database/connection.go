package database

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"strings"
)

type DB struct {
	Pool *pgxpool.Pool
}

func New(ctx context.Context, config *Config) (*DB, error) {
	pool, err := pgxpool.Connect(ctx, dbConnectionString(*config))

	if err != nil {
		return nil, fmt.Errorf("failed creating connection pool: %v", err)
	}

	return &DB{Pool: pool}, nil
}

func (db *DB) Close() {
	db.Pool.Close()
}

func dbConnectionString(config Config) string {
	vals := configToMap(config)
	var p []string
	for k, v := range vals {
		p = append(p, fmt.Sprintf("%s=%s", k, v))
	}
	return strings.Join(p, " ")
}
