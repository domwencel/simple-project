package database

import (
	"context"
	"github.com/sethvargo/go-envconfig"
	"log"
	"strconv"
	"time"
)

type Config struct {
	Name               string        `env:"DB_NAME, required"`
	User               string        `env:"DB_USER, required"`
	Host               string        `env:"DB_HOST, default=localhost"`
	Port               string        `env:"DB_PORT, default=5432"`
	SSLMode            string        `env:"DB_SSLMODE, required"`
	ConnectionTimeout  int           `env:"DB_CONNECT_TIMEOUT, required"`
	Password           string        `env:"DB_PASSWORD, default=pass"`
	PoolMinConnections string        `env:"DB_POOL_MIN_CONNS, required"`
	PoolMaxConnections string        `env:"DB_POOL_MAX_CONNS, required"`
	PoolMaxConnLife    time.Duration `env:"DB_POOL_MAX_CONN_LIFETIME, required"`
	PoolMaxConnIdle    time.Duration `env:"DB_POOL_MAX_CONN_IDLE_TIME, required"`
}

func NewConfig(ctx context.Context) *Config {
	var c Config
	err := envconfig.Process(ctx, &c)

	if err != nil {
		log.Fatalf("errors with loading env: %s", err)
	}

	return &c
}

func configToMap(config Config) map[string]string {
	m := map[string]string{}
	m["dbname"] = config.Name
	m["user"] = config.User
	m["host"] = config.Host
	m["port"] = config.Port
	m["sslmode"] = config.SSLMode
	m["connect_timeout"] = strconv.Itoa(config.ConnectionTimeout)
	m["password"] = config.Password
	m["pool_min_conns"] = config.PoolMinConnections
	m["pool_max_conns"] = config.PoolMaxConnections
	m["pool_max_conn_lifetime"] = config.PoolMaxConnLife.String()
	m["pool_max_conn_idle_time"] = config.PoolMaxConnIdle.String()
	return m
}
