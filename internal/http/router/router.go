package router

import (
	"github.com/gorilla/mux"
	"net/http"
	"simple-project/internal/http/app"
	"simple-project/internal/http/router/middleware"
)

func New(a *app.App) *mux.Router {
	r := mux.NewRouter()

	r.Use(middleware.JsonHeader)
	r.HandleFunc("/token/check", a.CheckToken()).Methods("POST")
	r.HandleFunc("/token/{userId}", a.GetToken()).Methods("GET")
	r.HandleFunc("/user/{token}", a.GetUser()).Methods("GET")
	r.HandleFunc("/user", a.CreateUser()).Methods("POST")
	r.HandleFunc("/user/byCredentials", a.AuthorizeByCredentials()).Methods("POST")
	r.HandleFunc("/test", test()).Methods("GET")

	return r
}

func test() func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("asdf"))
	}
}
