package app

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"net/http"
	"simple-project/internal/token"
	"simple-project/internal/validator"
)

func (a *App) GetUser() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		t, err := uuid.Parse(vars["token"])

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		u, err := a.userService.GetUserByToken(r.Context(), t)

		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		json.NewEncoder(w).Encode(u)
	}
}

func (a *App) CreateUser() func(w http.ResponseWriter, r *http.Request) {
	type request struct {
		Login string `json:"login" validate:"required"`
		Email string `json:"email" validate:"required,email"`
	}
	type response struct {
		Token  *token.Token `json:"token,omitempty"`
		Errors []error      `json:"errors,omitempty"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var req request

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		parsedErrors := validator.ParseError(a.validator.Struct(req))
		if len(parsedErrors) > 0 {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(response{Errors: parsedErrors})
			return
		}

		userId, err := a.userService.CreateUser(r.Context(), req.Email, req.Login)
		if err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		token, err := a.tokenService.CreateToken(r.Context(), userId)

		if err != nil {
			w.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		json.NewEncoder(w).Encode(token)

	}
}
