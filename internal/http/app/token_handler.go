package app

import (
	"encoding/json"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"simple-project/internal/token"
	"simple-project/internal/user"
	"simple-project/internal/validator"
)

type intFunction func(int) int

func Sort(intFunctions []intFunction) {
	for _, fn := range intFunctions {
		fn(1)
	}
}

func (a *App) CheckToken() func(w http.ResponseWriter, r *http.Request) {
	type request struct {
		Token string `json:"token" validate:"required,uuid4_rfc4122"`
	}
	type response struct {
		*user.User   `json:"user"`
		*token.Token `json:"token"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var req request

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Printf("error decoding: %s\n", err)
			w.WriteHeader(http.StatusUnprocessableEntity)
			return
		}

		parsedErrors := validator.ParseError(a.validator.Struct(req))
		if len(parsedErrors) > 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		ctx := r.Context()

		tokenId, _ := uuid.Parse(req.Token)
		user, err := a.userService.GetUserByToken(ctx, tokenId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		token, err := a.tokenService.GetOne(ctx, tokenId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if token.IsActive() {
			err := a.tokenService.UpdateTime(ctx, token.Id)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}

		json.NewEncoder(w).Encode(&response{
			User:  user,
			Token: token,
		})
	}
}

func (a *App) GetToken() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		userId := vars["userId"]
		uuid, err := uuid.Parse(userId)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		t, err := a.tokenService.GetOneByUserId(r.Context(), uuid)

		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		json.NewEncoder(w).Encode(t)
	}
}

func (a *App) Revoke() func(w http.ResponseWriter, r *http.Request) {
	type request struct {
		Token string `json:"token" validate:"required,uuid4_rfc4122"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var req request

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		parsedErrors := validator.ParseError(a.validator.Struct(req))
		if len(parsedErrors) > 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		tokenId, _ := uuid.Parse(req.Token)

		err = a.tokenService.Revoke(r.Context(), tokenId)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		}
	}
}
