package app

import (
	"github.com/go-playground/validator/v10"
	"simple-project/internal/token"
	"simple-project/internal/user"
)

type App struct {
	userService  *user.UserService
	tokenService token.TokenService
	validator    *validator.Validate
}

func NewApp(userService *user.UserService, tokenService token.TokenService, validator *validator.Validate) *App {
	return &App{
		userService:  userService,
		tokenService: tokenService,
		validator:    validator,
	}
}
