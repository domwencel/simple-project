package app

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/mail"
	"simple-project/internal/argon"
	"simple-project/internal/token"
	"simple-project/internal/user"
)

type request struct {
	Identity string `json:"identity" validate:"required, gte=0, lte=100"`
	Password string `json:"password" validate:"required, gte=0, lte=100"`
}

type identityType int

const (
	Email identityType = iota
	Login
)

func (r *request) getIdentityType() identityType {
	if _, err := mail.ParseAddress(r.Identity); err != nil {
		return Login
	}
	return Email
}

func (a *App) AuthorizeByCredentials() func(w http.ResponseWriter, r *http.Request) {

	type validationError struct {
		Message string `json:"message"`
	}

	type response struct {
		User  *user.User       `json:"user,omitempty"`
		Token *token.Token     `json:"token,omitempty"`
		Error *validationError `json:"errors,omitempty"`
	}

	return func(w http.ResponseWriter, r *http.Request) {
		ctx := context.Background()
		var req request

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			log.Printf("error decoding: %s\n", err)
			w.WriteHeader(http.StatusUnprocessableEntity)
			return
		}
		var u *user.User
		if Email == req.getIdentityType() {
			u, err = a.userService.GetUserByEmail(ctx, req.Identity)
		} else {
			u, err = a.userService.GetUserByLogin(ctx, req.Identity)
		}

		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusNotFound)
			return
		}

		t, err := a.tokenService.GetOneByUserId(ctx, u.Id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		if validPassword, _ := argon.ComparePassword(req.Password, u.Password); !validPassword {
			w.WriteHeader(http.StatusUnprocessableEntity)
			json.NewEncoder(w).Encode(&response{
				Error: &validationError{Message: "invalid password"},
			})
			return
		}

		if u.Status != user.ACTIVE {
			w.WriteHeader(http.StatusUnprocessableEntity)
			json.NewEncoder(w).Encode(&response{
				Error: &validationError{Message: "not active user"},
			})
			return
		}

		json.NewEncoder(w).Encode(&response{
			User:  u,
			Token: t,
		})
	}
}
