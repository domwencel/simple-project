module simple-project

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-playground/validator/v10 v10.3.0
	github.com/google/uuid v1.0.0
	github.com/gorilla/mux v1.7.4
	github.com/jackc/pgx/v4 v4.7.1
	github.com/joho/godotenv v1.3.0
	github.com/pkg/profile v1.5.0
	github.com/sethvargo/go-envconfig v0.2.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
