package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"log"
	"math/rand"
	"simple-project/internal/argon"
	"simple-project/internal/database"
	"simple-project/internal/user"
	"sync"
	"time"
)

func smain() {
	ctx := context.Background()
	db, err := database.New(ctx, database.NewConfig(ctx))
	if err != nil {
		log.Fatalf("errors with database: %s", err)
	}
	var wg sync.WaitGroup
	defer db.Close()
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go insert(db, ctx, &wg)
	}

	wg.Wait()
}

func insert(db *database.DB, ctx context.Context, wg *sync.WaitGroup) {
	conn, err := db.Pool.Acquire(ctx)
	if err != nil {
		panic(err)
	}
	defer wg.Done()
	defer db.Pool.Close()
	for i := 0; i < 10000; i++ {
		email := fmt.Sprintf("%s@%s", String(5), String(5))
		login := String(10)
		userId := uuid.New()
		token := uuid.New()
		plainPassword := String(10)
		password, err := argon.GeneratePassword(plainPassword)
		if err != nil {
			panic(err)
		}
		statuses := []user.UserStatus{user.UNACTIVATED, user.BANNED, user.ACTIVE, user.DELETED}
		index := rand.Intn(len(statuses))
		status := statuses[index]

		_, err = conn.Exec(
			ctx,
			"INSERT INTO users (id, login, email, updated_at, password, status, plain_password) VALUES($1, $2, $3, CURRENT_TIMESTAMP, $4, $5, $6)",
			userId,
			login,
			email,
			password,
			status,
			plainPassword,
		)
		if err != nil {
			log.Fatal(err)
		}
		_, err = conn.Exec(ctx, "INSERT INTO token VALUES($1, $2, $3)", token, userId, time.Now().Add(time.Hour))
		if err != nil {
			log.Fatal(err)
		}
	}
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func String(length int) string {
	return StringWithCharset(length, charset)
}
