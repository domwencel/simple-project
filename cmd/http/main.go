package main

import (
	"context"
	_ "github.com/joho/godotenv/autoload"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"simple-project/internal/database"
	"simple-project/internal/http/app"
	"simple-project/internal/http/router"
	"simple-project/internal/token"
	"simple-project/internal/user"
	"simple-project/internal/validator"
	"time"
)

func main() {
	ctx := context.Background()
	db, err := database.New(ctx, database.NewConfig(ctx))
	if err != nil {
		log.Fatalf("errors with database: %s", err)
	}
	defer db.Close()

	application := app.NewApp(user.NewUserService(db), token.NewTokenService(db), validator.New())

	r := router.New(application)

	srv := &http.Server{
		Handler:      r,
		Addr:         ":3001",
		WriteTimeout: 30 * time.Second,
		ReadTimeout:  30 * time.Second,
		IdleTimeout:  120 * time.Second,
	}

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		log.Println("Server is shutting down...")

		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		srv.SetKeepAlivesEnabled(false)
		if err := srv.Shutdown(ctx); err != nil {
			log.Fatalf("Could not gracefully shutdown the server: %v\n", err)
		}
	}()

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("Could not listen  %v\n", err)
	}
	log.Println("Server stopped")
}
